
#define _WINSOCKAPI_

#include <tempus/variant.hh>
#include <tempus/routing_data.hh>
#include <tempus/graph_builder.hh>
#include <tempus/common.hh>
#include <tempus/graph.hh>
#include <tempus/multimodal_routing.hh>

extern "C"
{

#include <limits.h>
#include <float.h>
#include <postgres.h>
#include <fmgr.h>
#include <funcapi.h>
#include <utils/elog.h>
#include <utils/builtins.h>
#include <utils/array.h>
#include <utils/guc.h>
#include <access/htup_details.h> // for heap_form_tuple
#include <datatype/timestamp.h> // for heap_form_tuple
#include <executor/spi.h>

#include <utils/lsyscache.h> // for get_typlenbyvalalign

PG_MODULE_MAGIC;

#ifdef _MSC_VER
#define DLL_EXPORT __declspec(dllexport)
#else
#define DLL_EXPORT
#endif

DLL_EXPORT void _PG_init(void);
DLL_EXPORT void _PG_fini(void);

DLL_EXPORT Datum tempus_loaded_graphs(PG_FUNCTION_ARGS);
DLL_EXPORT Datum tempus_unload_graph(PG_FUNCTION_ARGS);
DLL_EXPORT Datum tempus_build_multimodal_graph(PG_FUNCTION_ARGS);
DLL_EXPORT Datum tempus_one_to_many_paths(PG_FUNCTION_ARGS);
DLL_EXPORT Datum tempus_many_to_one_paths(PG_FUNCTION_ARGS);
DLL_EXPORT Datum tempus_set_static_road_section_costs(PG_FUNCTION_ARGS);
DLL_EXPORT Datum tempus_set_pt_section_timetable(PG_FUNCTION_ARGS);
DLL_EXPORT Datum tempus_set_arcs_sequence_costs(PG_FUNCTION_ARGS);
DLL_EXPORT Datum tempus_set_parking_costs(PG_FUNCTION_ARGS);
DLL_EXPORT Datum tempus_set_vehicles_positions(PG_FUNCTION_ARGS);
DLL_EXPORT Datum tempus_set_pt_boarding_security_times(PG_FUNCTION_ARGS);

DLL_EXPORT PG_FUNCTION_INFO_V1(tempus_loaded_graphs);
DLL_EXPORT PG_FUNCTION_INFO_V1(tempus_unload_graph);
DLL_EXPORT PG_FUNCTION_INFO_V1(tempus_build_multimodal_graph);
DLL_EXPORT PG_FUNCTION_INFO_V1(tempus_one_to_many_paths);
DLL_EXPORT PG_FUNCTION_INFO_V1(tempus_many_to_one_paths);
DLL_EXPORT PG_FUNCTION_INFO_V1(tempus_set_static_road_section_costs);
DLL_EXPORT PG_FUNCTION_INFO_V1(tempus_set_pt_section_timetable);
DLL_EXPORT PG_FUNCTION_INFO_V1(tempus_set_arcs_sequence_costs);
DLL_EXPORT PG_FUNCTION_INFO_V1(tempus_set_parking_costs);
DLL_EXPORT PG_FUNCTION_INFO_V1(tempus_set_vehicles_positions);
DLL_EXPORT PG_FUNCTION_INFO_V1(tempus_set_pt_boarding_security_times);

}


extern "C" {
bool pgtempus_debug_enabled = false;

#define DEBUG_MSG(...) if (pgtempus_debug_enabled) elog(NOTICE, __VA_ARGS__ )

void pgtempus_switch_debug( bool new_val, void* extra )
{
    pgtempus_debug_enabled = new_val;

    if ( pgtempus_debug_enabled ) {
        // output as NOTICE
        Tempus::set_out_printf_function( [](const char *s, int n) -> int
                                         {
                                             elog(NOTICE, "%s", s);
                                             return n;
                                         } );
        Tempus::set_err_printf_function( [](const char *s, int n) -> int
                                         {
                                             elog(NOTICE, "%s", s);
                                             return n;
                                         } );
    }
    else {
        Tempus::unset_out_printf_function();
        Tempus::unset_err_printf_function();
    }
}
}

extern "C" void _PG_init()
{
    //hstoreUpgradeP = (HStore* (*)(Datum))load_external_function("hstore", "hstoreUpgrade", /* signalNotfound */false, NULL);
    DEBUG_MSG("tempus_PG_init");
    // load plugins and graph builders
    tempus_init();

    DefineCustomBoolVariable( "pgtempus.debug", /* name */
                              "Sets the Debug level of Tempus.", /* short_desc */
                              "Sets the Debug level of Tempus (allowed values are 't' or 'f')", /* long_desc */
                              &pgtempus_debug_enabled, /* valueAddr */
                              false, /* bootValue */
                              PGC_USERSET, /* GucContext context */
                              0, /* int flags */
                              NULL, /* GucBoolCheckHook check_hook */
                              pgtempus_switch_debug, /* GucBoolAssignHook assign_hook */
                              NULL  /* GucShowHook show_hook */
                              );
}

extern "C" void _PG_fini()
{
    DEBUG_MSG("_pg_fini");
}

/**
 * It is not very safe to call elog(ERROR, ...) in a C++ context where we have C++ objects on the stack.
 * elog() will use a longjmp() and then no destructor will be called.
 *
 * We should then replace elog(ERROR, ...) with a C++ throw that correctly unwind the stack.
 * The calling C function must be surrounded by a try / catch block and call elog(ERROR, ...) for each C++ exception
 *
 * raise_error allow the use of a printf-like function to log errors and also exit from the current call stack.
 */
void raise_error( const char* fmt, ... )
{
    va_list args;
    int size = 0;
    char *p = NULL;
    va_list ap;

    /* Determine required size */

    va_start(ap, fmt);
    size = vsnprintf(p, size, fmt, ap);
    va_end(ap);

    if (size < 0)
        return;

    size++;             /* For '\0' */
    p = new char[size];
    if (p == NULL)
        return;

    va_start(ap, fmt);
    size = vsnprintf(p, size, fmt, ap);
    va_end(ap);

    std::string pmsg(p, size);
    delete[] p;
    throw std::runtime_error( pmsg );
}

#if 0
/**
 * Utilitary function to convert an hstore to a map of strings
 */
typedef std::map<std::string, std::string> StringMap;
static Tempus::VariantMap hstore_to_map( const HStore* hstore )
{
    Tempus::VariantMap ret_map;
    if ( hstore )
    {
        int count = HS_COUNT(hstore);
        const HEntry* arr_ptr = ARRPTR(hstore);
        const char* str_ptr = STRPTR(hstore);
        for ( int i = 0; i < count; i++ )
        {
            ret_map.emplace(std::make_pair(std::string(HSTORE_KEY(arr_ptr, str_ptr, i), HSTORE_KEYLEN(arr_ptr, i)),
                                           std::string(HSTORE_VAL(arr_ptr, str_ptr, i), HSTORE_VALLEN(arr_ptr, i))));
        }
    }
    return ret_map;
}
#endif

/**
 * Convert a one-dimensial array to a std::vector
 */
template <typename T>
static std::vector<T> array1_to_vector( const ArrayType* array )
{
    std::vector<T> vec;
    if ( array )
    {
        T *v = (T*)ARR_DATA_PTR(array);
        for ( int i = 0; i < ARR_DIMS(array)[0]; i++, v++ )
        {
            vec.push_back( *v );
        }
    }
    return vec;
}

/**
 * Converts a PostgreSQL's timestamp (int64) into a Tempus' datetime
 */
static Tempus::DateTime timestamp_to_datetime( Timestamp ts )
{
    // PostgreSQL epoch = 2000-01-01
    return Tempus::DateTime(Tempus::Date(2000,1,1)) + boost::posix_time::milliseconds( ts / 1000 );
}

/**
 * Graph information available for any in-memory graph
 */
struct GraphInfo
{
    std::unique_ptr<Tempus::RoutingData> graph;
    std::string dbstring;
    Tempus::VariantMap options;
};

typedef std::map<std::string, GraphInfo> GraphRegistry;

static void check_tuple_names_and_types( TupleDesc tuple_desc, const std::vector<std::pair<std::string, std::string>>& column_desc )
{
    std::map<std::string, std::string> columns;
    for ( int col = 1; col <= tuple_desc->natts; col++ ) {
        char *column_name = SPI_fname( tuple_desc, col );
        char *column_type = SPI_gettype( tuple_desc, col );
        columns[column_name] = column_type;
        pfree(column_name);
        pfree(column_type);
    }
    for ( const auto& p : column_desc ) {
        auto cit = columns.find( p.first );
        if ( cit == columns.end() ) {
            raise_error( "Cannot find column %s", p.first.c_str() );
        }
        if ( cit->second != p.second ) {
            raise_error( "Wrong type for column %s: expected %s, got %s", p.first.c_str(), p.second.c_str(), cit->second.c_str() );
        }
    }
}

/**
 * Main graph registry that persists in memory for the whole session
 */
static GraphRegistry g_graph_registry;

#if 0
extern "C" Datum tempus_build_graph(PG_FUNCTION_ARGS)
{
    const char* graph_id = text_to_cstring(PG_GETARG_TEXT_P(0));
    const char* dbstring = text_to_cstring(PG_GETARG_TEXT_P(1));
    const char *graph_type = PG_GETARG_POINTER(2) ?
        text_to_cstring(PG_GETARG_TEXT_P(2))
        : "multimodal_graph";
    const HStore *options = 0;
    if (PG_GETARG_POINTER(3))
    {
        options = PG_GETARG_HSTORE_P(3);
    }

    Tempus::VariantMap options_map = hstore_to_map( options );
    GraphInfo graph_info;
    graph_info.options = options_map;
    graph_info.dbstring = dbstring;
    if ( dbstring )
        options_map["db/options"] = Tempus::Variant( std::string( dbstring ) );
    try
    {
        std::unique_ptr<Tempus::RoutingData> rd = load_routing_data( graph_type, options_map );
        graph_info.graph = std::move( rd );
        g_graph_registry[graph_id] = std::move( graph_info );
    }
    catch ( std::exception& e )
    {
        elog(ERROR, "%s", e.what() );
    }
    PG_RETURN_VOID();
}
#endif


template <size_t ColumnCount>
class SPIRecordIterator : public Tempus::RecordIterator<std::array<std::pair<Datum, bool>, ColumnCount>>
{
public:
    using Row = std::array<std::pair<Datum, bool>, ColumnCount>;
    SPIRecordIterator( const char* query, const std::vector<std::pair<std::string, std::string>>& columns )
        : query_( query )
    {
        if ( SPI_connect() == SPI_ERROR_CONNECT )
            raise_error( "SPI_connect error" );
        SPIPlanPtr plan = SPI_prepare( query,
                                       /* nargs */ 0,
                                       /* argtypes */ NULL );
        if ( !plan )
            raise_error( "SPI_prepare error" );

        portal_ = SPI_cursor_open( /* name */ NULL, plan, /* values */ NULL, /* nulls */ NULL, /* read_only */ true );
        check_tuple_names_and_types( portal_->tupDesc, columns );

        for ( size_t col = 0; col < ColumnCount; col++ ) {
            col_idx_[col] = SPI_fnumber( portal_->tupDesc, columns[col].first.c_str() );
        }
    }

    ~SPIRecordIterator()
    {
        if ( buffer_.size() ) {
            SPI_freetuptable( SPI_tuptable );
        }
        SPI_cursor_close( portal_ );
        SPI_finish();
    }

    Tempus::Optional<Row> value() override
    {
        if ( index_ >= buffer_.size() )
        {
            populate_();
        }
        if ( index_ >= buffer_.size() )
            return {};
        return buffer_[index_];
    }

    bool next() override
    {
        if ( index_ < buffer_.size() ) {
            index_++;
            return true;
        }
        // else
        populate_();
        return index_ < buffer_.size();
    }

    size_t count() const override
    {
        if ( count_ == -1 ) {
            if ( SPI_connect() == SPI_ERROR_CONNECT )
                raise_error( "SPI_connect error" );
            if ( SPI_execute( ( std::string( "SELECT COUNT(*)::int8 FROM (" ) + query_ + ") __t__" ).c_str(),
                              /* read_only */ true,
                              /* count */ 1 ) != SPI_OK_SELECT )
                raise_error( "SPI_execute error" );
            bool isnull = false;
            count_ = DatumGetInt64(SPI_getbinval(SPI_tuptable->vals[0],
                                                 SPI_tuptable->tupdesc,
                                                 1,
                                                 &isnull));

            SPI_freetuptable( SPI_tuptable );
            SPI_finish();
        }
        return size_t(count_);
    }

    void populate_()
    {
        if ( finished_ )
            return;
        if ( buffer_.size() ) {
            SPI_freetuptable( SPI_tuptable );
            buffer_.clear();
        }
        SPI_cursor_fetch( portal_, /* forward */ true, fetch_count_ );
        for ( int row_idx = 0; row_idx < SPI_processed; row_idx++ ) {
            Row row;
            for ( size_t col = 0; col < ColumnCount; col++ ) {
                bool isnull = false;
                Datum dt = SPI_getbinval(SPI_tuptable->vals[row_idx],
                                            SPI_tuptable->tupdesc,
                                            col_idx_[col],
                                            &isnull);
                row[col] = std::make_pair( dt, isnull );
            }
            buffer_.push_back( row );
        }
        finished_ = buffer_.size() < fetch_count_;
        index_ = 0;
    }

private:
    const char* query_;
    Portal portal_;
    std::vector<Row> buffer_;
    size_t index_ = 0;
    size_t fetch_count_ = 1024;
    std::array<int, ColumnCount> col_idx_;
    bool finished_ = false;
    mutable int64_t count_ = -1;
};

template <typename Record, size_t ColumnCount>
class RecordIteratorConverter : public Tempus::RecordIterator<Record>
{
public:
    using Row = std::array<std::pair<Datum, bool>, ColumnCount>;

    RecordIteratorConverter( const char* query, const std::vector<std::pair<std::string, std::string>>& columns, std::function<Record(const Row&)> converter ) :
        spi_it_( query, columns )
        , converter_( converter )
    {
        assert( columns.size() == ColumnCount );
    }

    virtual Tempus::Optional<Record> value() override
    {
        Tempus::Optional<Row> dt = spi_it_.value();
        if ( dt ) {
            return converter_( *dt );
        }
        return {};
    }
    size_t count() const override { return spi_it_.count(); }
    bool next() override { return spi_it_.next(); }
private:
    SPIRecordIterator<ColumnCount> spi_it_;
    std::function<Record(const Row&)> converter_;
};

template <typename Record, size_t ColumnCount>
RecordIteratorConverter<Record, ColumnCount> make_record_iterator_converter( const char *query, const std::vector<std::pair<std::string, std::string>>& columns, std::function<Record(const std::array<std::pair<Datum, bool>, ColumnCount>&)> converter )
{
    return RecordIteratorConverter<Record, ColumnCount>( query, columns, converter );
}

uint32_t rules_vector_to_bitfield( const std::vector<uint16_t>& rules_v )
{
    uint32_t rules = 0;
    for ( uint16_t r : rules_v ) {
        if ( r < 1 ) {
            elog(WARNING, "Cannot have a rule ID < 1, ignoring %d", r);
            continue;
        }
        if ( r > 32 ) {
            elog(WARNING, "Cannot have a rule ID > 32, ignoring %d", r);
            continue;
        }
        rules |= 1 << (r-1);
    }
    return rules;
}

uint32_t rule_to_bitfield( const uint16_t& r) 
{
    uint32_t rule = 0;
    if (r == 0) rule = 0;
    else {
        if ( r < 1 ) {
            elog(WARNING, "Cannot have a rule ID < 1, ignoring %d", r);
        }
        if ( r > 32 ) {
            elog(WARNING, "Cannot have a rule ID > 32, ignoring %d", r);
        }
        rule |= 1 << (r-1);
    }
    return rule;
}

extern "C" Datum tempus_build_multimodal_graph(PG_FUNCTION_ARGS)
{
    using namespace Tempus;
    static const std::map<std::string, TransportMode::Category> transport_mode_mapping = {
                                                                                          {"Walking", TransportMode::Walking},
                                                                                          {"Car", TransportMode::Car},
                                                                                          {"Bicycle", TransportMode::Bicycle}
    };
    
    if ( !PG_GETARG_POINTER(0) || PG_ARGISNULL(0) )
        raise_error( "A non null graph name is needed" );        
    const char* graph_id = text_to_cstring(PG_GETARG_TEXT_P(0));
    
    if ( !PG_GETARG_POINTER(1) || PG_ARGISNULL(1) )
        raise_error( "A non null transport modes query is needed" );
    const char* transport_modes_query = text_to_cstring(PG_GETARG_TEXT_P(1));
    
    if ( !PG_GETARG_POINTER(2) || PG_ARGISNULL(2) )
        raise_error( "A non null nodes query is needed" );
    const char* nodes_query = text_to_cstring(PG_GETARG_TEXT_P(2));
    
    if ( !PG_GETARG_POINTER(3) || PG_ARGISNULL(3) )
        raise_error( "A non null arc query is needed" );
    const char* arcs_query = text_to_cstring(PG_GETARG_TEXT_P(3));

    try
    {
        std::function<TransportMode( const std::array<std::pair<Datum, bool>, 6>& )> tm_converter = [&]( const std::array<std::pair<Datum, bool>, 6>& row ) -> Tempus::TransportMode // Note: change columns number in the function signature if necessary
        { 
            Tempus::TransportMode tm;
            tm.set_db_id( DatumGetInt16( row[0].first ) );
            tm.set_name( text_to_cstring( DatumGetTextP( row[1].first ) ) );
            std::string category_str = text_to_cstring( DatumGetTextP( row[2].first ) );
            auto cat_it = transport_mode_mapping.find( category_str );
            tm.set_category( cat_it == transport_mode_mapping.end() ? Tempus::TransportMode::Walking : cat_it->second );              
            tm.set_traffic_rule( rule_to_bitfield( DatumGetInt16( row[3].first ) ) );        
            if ( !row[4].second ) tm.set_toll_rule( rule_to_bitfield( DatumGetInt16( row[4].first ) ) );
            else tm.set_toll_rule( 0 );
            if ( !row[5].second ) tm.set_vehicle_parking_rule( rule_to_bitfield( DatumGetInt16( row[5].first ) ) );
            else tm.set_vehicle_parking_rule( 0 );
            
            return tm;                            
        };
        auto tm_it = make_record_iterator_converter( transport_modes_query,
                                                     {{"id", "int2"},
                                                      {"name", "text"},
                                                      {"category", "text"},
                                                      {"traffic_rule", "int2"}, 
                                                      {"toll_rule", "int2"}, 
                                                      {"vehicle_parking_rule", "int2"}
                                                     },
                                                     tm_converter );

        std::function<Tempus::Node( const std::array<std::pair<Datum, bool>, 4>& )> node_converter = [&]( const std::array<std::pair<Datum, bool>, 4>& row ) -> Tempus::Node
        {
            Tempus::Node node;
            node.set_db_id( DatumGetInt64( row[0].first ) );
            if ( row[1].second && row[2].second )
            {
                Point3D p;
                p.set_x( DatumGetFloat4( row[1].first ) );
                p.set_y( DatumGetFloat4( row[2].first ) );
                if ( row[3].second )
                    p.set_z( DatumGetFloat4( row[3].first ) );
                node.set_coordinates( p );
            }
            return node;
        };
        auto node_it = make_record_iterator_converter( nodes_query,
                                                       {{"id", "int8"},
                                                        {"x", "float4"},
                                                        {"y", "float4"},
                                                        {"z", "float4"}},
                                                       node_converter );

        std::function<ArcRecord( const std::array<std::pair<Datum, bool>, 5>& )> arc_converter = [&]( const std::array<std::pair<Datum, bool>, 5>& row ) -> ArcRecord
        {
            Tempus::ArcRecord er;
            er.arc.set_db_id( DatumGetInt64( row[0].first ) );
            er.node_from_id = DatumGetInt64( row[1].first );
            er.node_to_id = DatumGetInt64( row[2].first );
            if ( DatumGetInt16( row[3].first ) == 1 ) {
                er.arc.set_type( Arc::RoadArc );
            }
            else if ( DatumGetInt16( row[3].first ) == 2 ) { 
                er.arc.set_type( Arc::PublicTransportArc );
            }
            else {
                elog( WARNING, "Arc with type = %lld not recognized, supposed to be a road arc", DatumGetInt16( row[3].first ) );
                er.arc.set_type( Arc::RoadArc );
            }
            if ( ! row[4].second ) {
                er.arc.set_traffic_rules( rules_vector_to_bitfield( array1_to_vector<uint16_t>( DatumGetArrayTypeP( row[4].first ) ) ) );
            }
            return er;
        };

        auto arc_it = make_record_iterator_converter( arcs_query,
                                                       {{"id", "int8"},
                                                        {"node_from_id", "int8"},
                                                        {"node_to_id", "int8"},
                                                        {"type", "int2"},
                                                        {"traffic_rules", "_int2"} // array of int2
                                                       },
                                                       arc_converter );

        GraphInfo graph_info;
        std::unique_ptr<Tempus::RoutingData> rd = Tempus::GraphBuilder::build_from_iterators( tm_it, node_it, arc_it );
        graph_info.graph = std::move( rd );
        g_graph_registry[graph_id] = std::move( graph_info );
    }
    catch ( std::exception& e )
    {
        elog(ERROR, "%s", e.what() );
    }
    PG_RETURN_VOID();
}

extern "C" Datum tempus_loaded_graphs(PG_FUNCTION_ARGS)
{
    try {
        FuncCallContext     *funcctx;
        int                  call_cntr;
        int                  max_calls;
        TupleDesc            tupdesc;

        /* stuff done only on the first call of the function */
        if (SRF_IS_FIRSTCALL())
        {
            MemoryContext   oldcontext;

            /* create a function context for cross-call persistence */
            funcctx = SRF_FIRSTCALL_INIT();

            /* switch to memory context appropriate for multiple function calls */
            oldcontext = MemoryContextSwitchTo(funcctx->multi_call_memory_ctx);

            /* Build a tuple descriptor for our result type */
            if (get_call_result_type(fcinfo, NULL, &funcctx->tuple_desc) != TYPEFUNC_COMPOSITE)
                ereport(ERROR,
                        (errcode(ERRCODE_FEATURE_NOT_SUPPORTED),
                         errmsg("function returning record called in context "
                                "that cannot accept type record")));

            funcctx->tuple_desc = BlessTupleDesc(funcctx->tuple_desc);

            /*
             * generate attribute metadata needed later to produce tuples from raw
             * C strings
             */
            funcctx->attinmeta = TupleDescGetAttInMetadata(funcctx->tuple_desc);

            //allocate an iterator over the graph registry
            void* reg_iterator = palloc(sizeof(GraphRegistry::const_iterator));
            *reinterpret_cast<GraphRegistry::const_iterator *>(reg_iterator) = g_graph_registry.begin();
            funcctx->user_fctx = reg_iterator;
            
            funcctx->max_calls = g_graph_registry.size();

            MemoryContextSwitchTo(oldcontext);
        }

        /* stuff done on every call of the function */
        funcctx = SRF_PERCALL_SETUP();

        if (funcctx->call_cntr < funcctx->max_calls)    /* do when there is more left to send */
        {
            HeapTuple    tuple;
            Datum        result;

            GraphRegistry::const_iterator *reg_it = reinterpret_cast<GraphRegistry::const_iterator*>( funcctx->user_fctx );

            Datum values[4] = { 0, 0, 0, 0 };
            bool isnull[4] = { false, false, false, false };

            // id
            values[0] = CStringGetTextDatum( (*reg_it)->first.c_str() );
            // dbstring
            values[1] = CStringGetTextDatum( (*reg_it)->second.dbstring.c_str() );
            // graph_type
            values[2] = CStringGetTextDatum( (*reg_it)->second.graph->name().c_str() );
            // options
            std::string map_str;
            bool first = true;
            for ( const auto& p: (*reg_it)->second.options )
            {
                if ( ! first )
                    map_str += ",";
                map_str += p.first + "=>" + p.second.str();
                first = false;
            }
            values[3] = CStringGetTextDatum( map_str.c_str() );

            tuple = heap_form_tuple(funcctx->tuple_desc, values, isnull);

            // increment iterator
            (*reg_it)++;

            /* make the tuple into a datum */
            result = HeapTupleGetDatum(tuple);

            SRF_RETURN_NEXT(funcctx, result);
        }
        else    /* do when there is no more left */
        {
            SRF_RETURN_DONE(funcctx);
        }
    }
    catch ( std::exception& e )
    {
        elog(ERROR, "%s", e.what());
    }
}

extern "C" Datum tempus_unload_graph(PG_FUNCTION_ARGS)
{
    const char* graph_id = text_to_cstring(PG_GETARG_TEXT_P(0));
    auto it = g_graph_registry.find( graph_id );
    if ( it != g_graph_registry.end() )
    {
        g_graph_registry.erase( it );
    }
    return 0;
}

/**
 * Allocate an Interval and fills it from a number of seconds
 */
static Interval* seconds_to_interval( uint32_t seconds )
{
    Interval* itv = (Interval*)palloc(sizeof(Interval));
    std::memset(itv, 0, sizeof(Interval));
    itv->time = (int64_t)seconds * 1000000;
    return itv;
}

Datum tempus_paths( FunctionCallInfo& fcinfo, bool is_direction_forward )
{
    try
    {
        FuncCallContext     *funcctx;

        /* stuff done only on the first call of the function */
        if (SRF_IS_FIRSTCALL())
        {
            MemoryContext   oldcontext;

            /* create a function context for cross-call persistence */
            funcctx = SRF_FIRSTCALL_INIT();

            /* switch to memory context appropriate for multiple function calls */
            oldcontext = MemoryContextSwitchTo(funcctx->multi_call_memory_ctx);

            /* Build a tuple descriptor for our result type */
            if (get_call_result_type(fcinfo, NULL, &funcctx->tuple_desc) != TYPEFUNC_COMPOSITE)
                ereport(ERROR,
                        (errcode(ERRCODE_FEATURE_NOT_SUPPORTED),
                         errmsg("function returning record called in context "
                                "that cannot accept type record")));

            funcctx->tuple_desc = BlessTupleDesc(funcctx->tuple_desc);

            /*
             * generate attribute metadata needed later to produce tuples from raw
             * C strings
             */
            funcctx->attinmeta = TupleDescGetAttInMetadata(funcctx->tuple_desc);

            if ( !PG_GETARG_POINTER(0) || PG_ARGISNULL(0) )
                raise_error( "A graph name is needed" );
            
            const char* graph_id = text_to_cstring(PG_GETARG_TEXT_P(0));
            auto reg_it = g_graph_registry.find( graph_id );
            if ( reg_it == g_graph_registry.end() ) {
                raise_error( "Graph %s not loaded !", graph_id );
            }
            
            if ( !PG_GETARG_POINTER(1) || PG_ARGISNULL(1) )
                raise_error( "An algorithm name is needed" );
            
            const Tempus::RoutingData* graph = reg_it->second.graph.get();
            const char* algo_name = text_to_cstring(PG_GETARG_TEXT_P(1));
            if ( std::string( algo_name ) != "multimodal_ls" ) {
                raise_error( "Unknown algorithm %s. Available algorithms: multimodal_ls", algo_name );
            }
            if ( reg_it->second.graph->name() != "multimodal_graph" ) {
                raise_error( "Wrong graph type %s, expected: multimodal_graph", reg_it->second.graph->name().c_str() );
            }
            
            if ( !PG_GETARG_POINTER(2) || PG_ARGISNULL(2) )
                raise_error( "A starting node is needed" );

            Tempus::Request req;
            
            Tempus::db_id_t start_node = PG_GETARG_INT64(2);
            DEBUG_MSG("Starting node = %lld", start_node);
            
            req.set_starting_node( start_node );
            
            std::vector<Tempus::db_id_t> end_nodes;
            if ( !PG_ARGISNULL(3) )
            {
                end_nodes = array1_to_vector<Tempus::db_id_t>(PG_GETARG_ARRAYTYPE_P(3));
                DEBUG_MSG("Ending node = ");
                for ( Tempus::db_id_t node_id : end_nodes ) {
                    DEBUG_MSG( "  %lld", node_id ); 
                    req.add_ending_node( node_id );
                }
            }
            
            if ( !PG_GETARG_POINTER(4) )
                raise_error( "A starting transport mode is needed" );
            
            Tempus::db_id_t starting_transport_mode = PG_GETARG_INT16(4);
            DEBUG_MSG("Starting transport mode = %lld", starting_transport_mode);
            req.set_starting_transport_mode( starting_transport_mode );
                        
            Tempus::db_id_t ending_transport_mode = PG_GETARG_INT16(5);
            DEBUG_MSG("Final transport mode = %lld", ending_transport_mode);
            req.set_ending_transport_mode( ending_transport_mode );
            
            Tempus::Optional<float> max_cost;
            if ( ! PG_ARGISNULL(6) ) {
                max_cost = PG_GETARG_FLOAT4(6);
                DEBUG_MSG("Maximum generalized cost: %f", max_cost.get());
            }
            
            if ( end_nodes.empty() && ! max_cost )
                raise_error( "There should be either an ending node or a maximum cost set" );
            
            req.set_maximum_cost( max_cost );
            
            // allowed_transport_mode_ids
            std::vector<Tempus::db_id_t> allowed_transport_mode_ids;
            if ( ! PG_ARGISNULL(7) ) {
                allowed_transport_mode_ids = array1_to_vector<Tempus::db_id_t>(PG_GETARG_ARRAYTYPE_P(7));
                DEBUG_MSG("Allowed transport modes = ");
                for ( Tempus::db_id_t transport_mode_id : allowed_transport_mode_ids ) {
                    DEBUG_MSG("  %lld", transport_mode_id );
                    req.add_allowed_transport_mode( transport_mode_id );
                }
            }
            
            // time_constraint and time_constraint_type
            if ( ! PG_ARGISNULL(8) ) {
                req.set_time_constraint_type( is_direction_forward ? Tempus::Request::ConstraintAfter : Tempus::Request::ConstraintBefore );
                req.set_time_constraint( timestamp_to_datetime( PG_GETARG_INT64(8) ) );                
            }
            
            // pt_time_weight
            if ( !PG_GETARG_POINTER(9) )
                raise_error( "A PT time weight is needed" );
            
            float pt_time_weight = PG_GETARG_FLOAT4(9);
            DEBUG_MSG("PT time weight = %f", pt_time_weight);
            req.set_pt_time_weight( pt_time_weight );
            
            float waiting_time_weight = PG_GETARG_FLOAT4(10);
            DEBUG_MSG("Waiting time weight = %f", waiting_time_weight);
            req.set_waiting_time_weight( waiting_time_weight );
            
            float indiv_mode_time_weight = PG_GETARG_FLOAT4(11);
            DEBUG_MSG("Individual mode time weight = %f", indiv_mode_time_weight);
            req.set_indiv_mode_time_weight( indiv_mode_time_weight );
            
            float fare_weight = PG_GETARG_FLOAT4(12);
            DEBUG_MSG("Fare weight = %f", fare_weight);
            req.set_fare_weight( fare_weight );
            
            float pt_transfer_weight = PG_GETARG_FLOAT4(13);
            DEBUG_MSG("PT transfer weight = %f", pt_transfer_weight);
            req.set_pt_transfer_weight( pt_transfer_weight );
            
            float mode_change_weight = PG_GETARG_FLOAT4(14);
            DEBUG_MSG("Mode change weight = %f", mode_change_weight);
            req.set_mode_change_weight( mode_change_weight );
            
            // max_walking_time
            if ( ! PG_ARGISNULL(15) ) {
                DEBUG_MSG("Max walking time = %lld seconds", PG_GETARG_INT32(15)/1000000);
                req.set_max_walking_time( PG_GETARG_INT32(15)/1000000 );
            }
            
            // max_bicycle_time
            if ( ! PG_ARGISNULL(16) ) {
                DEBUG_MSG("Max bicycle time = %lld seconds", PG_GETARG_INT32(16)/1000000);
                req.set_max_bicycle_time( PG_GETARG_INT32(16)/1000000 );
            }
            
            // max_pt_transfers
            if ( ! PG_ARGISNULL(17) ) {
                int max_pt_transfers = PG_GETARG_INT16(17);
                DEBUG_MSG( "Max PT transfers = %lld", max_pt_transfers );
                req.set_max_pt_transfers( max_pt_transfers );
            }
            
            // max_mode_changes
            if ( ! PG_ARGISNULL(18) ) {
                int max_mode_changes = PG_GETARG_INT16(18);
                DEBUG_MSG( "Max mode changes = %lld", max_mode_changes );
                req.set_max_mode_changes( max_mode_changes );
            }
            
            DEBUG_MSG(is_direction_forward ? "Leave after..." : "Arrive before..." );
            
            Tempus::Steps* pctxt = new Tempus::Steps();        
            *pctxt = Tempus::multimodal_routing( *static_cast<const Tempus::Graph*>( graph ), req );
        
            /* total number of tuples to be returned */
            funcctx->max_calls = pctxt->size();
            //DEBUG_MSG( "Total number of tuples to be returned = %lld", funcctx->max_calls );
            funcctx->user_fctx = pctxt;

            MemoryContextSwitchTo( oldcontext );
        }
        
        /* stuff done on every call of the function */
        funcctx = SRF_PERCALL_SETUP();
        if (funcctx->call_cntr < funcctx->max_calls)    /* do when there are still records to return */
        {
            HeapTuple    tuple;
            Datum        result;

            Tempus::Steps* pctxt = (Tempus::Steps*)( funcctx->user_fctx );
            
            Datum values[20];
            bool isnull[20] = { false, true, true, false, true, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false };
            
            const Tempus::Step& step = (*pctxt)[funcctx->call_cntr];
            values[0] = Int64GetDatum(step.id);
            if (step.pred_id > 0) {
                values[1] = Int64GetDatum(step.pred_id);
                isnull[1] = false;
            }
            if ( step.arc_id > 0) {
                values[2] = Int64GetDatum(step.arc_id);
                isnull[2] = false;
            }
            values[3] = Int64GetDatum(step.node_id);
            if (step.pred_node_id > 0 ) {
                values[4] = Int64GetDatum(step.pred_node_id);
                isnull[4] = false;
            }
            values[5] = Int16GetDatum(step.transport_mode_id);
            values[6] = Int32GetDatum(step.automaton_state);
            values[7] = Float4GetDatum(step.cost);
            values[8] = Float4GetDatum(step.pred_cost);
            if ( step.pt_trip_id > 0 )
            {
                values[9] = Int64GetDatum(step.pt_trip_id);
                isnull[9] = false;
            }
            if (isnull[1] == true) isnull[10] = true;
            else values[10] = PointerGetDatum( seconds_to_interval(step.waiting_time) );
            if (isnull[1] == true) isnull[11] = true;
            else values[11] = PointerGetDatum( seconds_to_interval(step.departure_time) );
            if (isnull[1] == true) isnull[12] = true;
            else values[12] = PointerGetDatum( seconds_to_interval(step.arrival_time) );
            values[13] = PointerGetDatum( seconds_to_interval(step.total_time) );
            values[14] = PointerGetDatum( seconds_to_interval(step.total_walking_time) );
            values[15] = PointerGetDatum( seconds_to_interval(step.total_bicycle_time) );
            values[16] = Int16GetDatum( step.total_pt_transfers );
            values[17] = Int16GetDatum( step.total_mode_changes );
            values[18] = Float4GetDatum( step.total_fare );
            values[19] = BoolGetDatum( step.in_paths );
            tuple = heap_form_tuple(funcctx->tuple_desc, values, isnull);
            
            /* make the tuple into a datum */
            result = HeapTupleGetDatum(tuple);
            
            SRF_RETURN_NEXT(funcctx, result);
        }
        else    /* do when there are no more records to return */
        {    
            Tempus::Steps* pctxt = (Tempus::Steps*)( funcctx->user_fctx );
            // we assume this code part is called even when a backend exception is raised
            delete pctxt;
            SRF_RETURN_DONE(funcctx);
        }
    }
    catch ( std::exception& e )
    {
        elog(ERROR, "%s", e.what());
    }
}

extern "C" Datum tempus_one_to_many_paths(PG_FUNCTION_ARGS)
{
    return tempus_paths( fcinfo, /* is_direction_forward */ true );
}

extern "C" Datum tempus_many_to_one_paths(PG_FUNCTION_ARGS)
{
    return tempus_paths( fcinfo, /* is_direction_forward */ false );
}

extern "C" Datum tempus_set_static_road_section_costs(PG_FUNCTION_ARGS)
{
    try {
        if ( !PG_GETARG_POINTER(0) || PG_ARGISNULL(0) )
            raise_error( "A non null graph name is needed" );
        
        const char* graph_id = text_to_cstring(PG_GETARG_TEXT_P(0));
        auto reg_it = g_graph_registry.find( graph_id );
        if ( reg_it == g_graph_registry.end() ) {
            raise_error( "Graph %s not loaded !", graph_id );
        }

        Tempus::Graph* graph = static_cast<Tempus::Graph*>( reg_it->second.graph.get() );

        if ( !PG_GETARG_POINTER(1) || PG_ARGISNULL(1) )
            raise_error( "A non null query is needed" );
        
        const char* query = text_to_cstring(PG_GETARG_TEXT_P(1));

        if ( SPI_connect() == SPI_ERROR_CONNECT )
            raise_error( "SPI_connect error" );

        SPIPlanPtr plan = SPI_prepare( query,
                                       /* nargs */ 0,
                                       /* argtypes */ NULL );
        if ( !plan )
            raise_error( "SPI_prepare error" );

        Portal portal = SPI_cursor_open( /* name */ NULL, plan, /* values */ NULL, /* nulls */ NULL, /* read_only */ true );

        const TupleDesc& tuple_desc = portal->tupDesc;
        check_tuple_names_and_types( tuple_desc, {
                                                  {"arc_id", "int8"},
                                                  {"transport_mode_id", "int2"},
                                                  {"travel_time", "int4"},
                                                  {"fare", "float4"}
            } );

        size_t fetch_count = 1024;

        const int arc_id_col_idx = SPI_fnumber( tuple_desc, "arc_id" );
        const int transport_mode_id_col_idx = SPI_fnumber( tuple_desc, "transport_mode_id" );
        const int travel_time_col_idx = SPI_fnumber( tuple_desc, "travel_time" );
        const int fare_col_idx = SPI_fnumber( tuple_desc, "fare" );

        do {
            SPI_cursor_fetch( portal, /* forward */ true, fetch_count );
            for ( int row = 0; row < SPI_processed; row++ ) {
                bool isnull = false;
                Tempus::db_id_t arc_id = DatumGetUInt64( SPI_getbinval(SPI_tuptable->vals[row],
                                                                        SPI_tuptable->tupdesc,
                                                                        arc_id_col_idx,
                                                                        &isnull) );
                Tempus::db_id_t transport_mode_id = DatumGetUInt64( SPI_getbinval(SPI_tuptable->vals[row],
                                                                        SPI_tuptable->tupdesc,
                                                                        transport_mode_id_col_idx,
                                                                        &isnull) );
                
                int32_t travel_time = DatumGetInt32( SPI_getbinval(SPI_tuptable->vals[row],
                                                               SPI_tuptable->tupdesc,
                                                               travel_time_col_idx,
                                                               &isnull) );
                float fare = DatumGetFloat4( SPI_getbinval(SPI_tuptable->vals[row],
                                                               SPI_tuptable->tupdesc,
                                                               fare_col_idx,
                                                               &isnull) );
                //elog(NOTICE, "arc_id %lld mode_id %lld cost %f cost_ts %lld", arc_id, mode_id, cost, cost_ts);

                Tempus::Optional<Tempus::Graph::edge_descriptor> arc = graph->arc_from_id( arc_id );
                if ( ! arc ) {
                    elog( WARNING, "Cannot find arc with ID = %lld in memory, skipping", arc_id );
                    continue;
                }
                graph->set_static_road_section_cost( arc.get(), transport_mode_id, travel_time, fare );
            }

            SPI_freetuptable( SPI_tuptable );

        } while ( SPI_processed == fetch_count );

        SPI_cursor_close( portal );


        SPI_finish();
    }
    catch ( std::exception& e )
    {
        elog(ERROR, "%s", e.what());
    }
    
    PG_RETURN_VOID();
}

extern "C" Datum tempus_set_pt_section_timetable(PG_FUNCTION_ARGS)
{
    try 
    {
        if ( !PG_GETARG_POINTER(0) || PG_ARGISNULL(0) )
            raise_error( "A non null graph name is needed" );
        
        const char* graph_id = text_to_cstring(PG_GETARG_TEXT_P(0));
        auto reg_it = g_graph_registry.find( graph_id );
        if ( reg_it == g_graph_registry.end() ) {
            raise_error( "Graph %s not loaded !", graph_id );
        }

        Tempus::Graph* graph = static_cast<Tempus::Graph*>( reg_it->second.graph.get() );
        
        if ( !PG_GETARG_POINTER(2) || PG_ARGISNULL(2) )
            raise_error( "A non null time origin for the graph is needed" );
        
        DEBUG_MSG("Time origin for the graph = %lld seconds", PG_GETARG_INT64(2)/1000000 );
        graph->set_time_origin( timestamp_to_datetime( PG_GETARG_INT64(2) ) );

        if ( !PG_GETARG_POINTER(1) || PG_ARGISNULL(1) )
            raise_error( "A non null query is needed" );
        
        const char* query = text_to_cstring(PG_GETARG_TEXT_P(1));
        
        if ( SPI_connect() == SPI_ERROR_CONNECT )
            raise_error( "SPI_connect error" );

        SPIPlanPtr plan = SPI_prepare( query,
                                       /* nargs */ 0,
                                       /* argtypes */ NULL );
        if ( !plan )
            raise_error( "SPI_prepare error" );

        Portal portal = SPI_cursor_open( /* name */ NULL, plan, /* values */ NULL, /* nulls */ NULL, /* read_only */ true );

        const TupleDesc& tuple_desc = portal->tupDesc;
        check_tuple_names_and_types( tuple_desc, {
                                                  {"arc_id", "int8"},
                                                  {"departure", "interval"},
                                                  {"arrival", "interval"},
                                                  {"pt_trip_id", "int8"},
                                                  {"traffic_rules", "_int2"} // array of int2
        } );
        size_t fetch_count = 1024;

        const int arc_id_col_idx = SPI_fnumber( tuple_desc, "arc_id" );
        const int departure_col_idx = SPI_fnumber( tuple_desc, "departure" );
        const int arrival_col_idx = SPI_fnumber( tuple_desc, "arrival" );
        const int pt_trip_id_col_idx = SPI_fnumber( tuple_desc, "pt_trip_id" );
        const int traffic_rules_col_idx = SPI_fnumber( tuple_desc, "traffic_rules" );
        
        graph->clear_pt_timetable();

        do {
            SPI_cursor_fetch( portal, /* forward */ true, fetch_count );
            for ( int row = 0; row < SPI_processed; row++ ) {
                bool isnull = false;
                Tempus::db_id_t arc_id = DatumGetUInt64( SPI_getbinval(SPI_tuptable->vals[row],
                                                                        SPI_tuptable->tupdesc,
                                                                        arc_id_col_idx,
                                                                        &isnull) );
                
                Tempus::db_id_t pt_trip_id = DatumGetUInt64( SPI_getbinval(SPI_tuptable->vals[row],
                                                                        SPI_tuptable->tupdesc,
                                                                        pt_trip_id_col_idx,
                                                                        &isnull) );
                Interval* departure = (Interval*)DatumGetPointer( SPI_getbinval(SPI_tuptable->vals[row],
                                                               SPI_tuptable->tupdesc,
                                                               departure_col_idx,
                                                               &isnull) );
                Interval* arrival = (Interval*)DatumGetPointer( SPI_getbinval(SPI_tuptable->vals[row],
                                                               SPI_tuptable->tupdesc,
                                                               arrival_col_idx,
                                                               &isnull) ); 
                
                int32_t dep_s = departure->time / 1000000; 
                int32_t arr_s = arrival->time / 1000000;

                ArrayType* array = DatumGetArrayTypeP( SPI_getbinval(SPI_tuptable->vals[row],
                                                                     SPI_tuptable->tupdesc,
                                                                     traffic_rules_col_idx,
                                                                     &isnull) );
                if ( isnull )
                    continue;

                uint32_t traffic_rules = rules_vector_to_bitfield( array1_to_vector<uint16_t>( array ) );


                Tempus::Optional<Tempus::Graph::edge_descriptor> arc = graph->arc_from_id( arc_id );
                if ( ! arc ) {
                    elog( WARNING, "Cannot find arc with ID = %lld in memory, skipping", arc_id );
                    continue;
                }
                graph->add_to_pt_timetable( *arc, Tempus::Timetable::TripTime( dep_s, arr_s, pt_trip_id, traffic_rules ) );
            }
            
            SPI_freetuptable( SPI_tuptable );

        } while ( SPI_processed == fetch_count );

        SPI_cursor_close( portal );

        SPI_finish();

        graph->sort_pt_timetable();
    }
    catch ( std::exception& e )
    {
        elog(ERROR, "%s", e.what());
    }
    
    PG_RETURN_VOID();
}

extern "C" Datum tempus_set_arcs_sequence_costs(PG_FUNCTION_ARGS)
{
    try {
        if ( !PG_GETARG_POINTER(0) || PG_ARGISNULL(0) )
            raise_error( "A non null graph name is needed" );
        
        const char* graph_id = text_to_cstring(PG_GETARG_TEXT_P(0));
        auto reg_it = g_graph_registry.find( graph_id );
        if ( reg_it == g_graph_registry.end() ) {
            raise_error( "Graph %s not loaded !", graph_id );
        }
        if ( reg_it->second.graph->name() != "multimodal_graph" ) {
            raise_error( "Wrong graph type %s, expected: multimodal_graph", reg_it->second.graph->name().c_str() );
        }

        Tempus::Graph* graph = static_cast<Tempus::Graph*>( reg_it->second.graph.get() );

        if ( !PG_GETARG_POINTER(1) || PG_ARGISNULL(1) )
            raise_error( "A non null query is needed" );
        
        const char* query = text_to_cstring(PG_GETARG_TEXT_P(1));

        if ( SPI_connect() == SPI_ERROR_CONNECT )
            raise_error( "SPI_connect error" );

        SPIPlanPtr plan = SPI_prepare( query,
                                       /* nargs */ 0,
                                       /* argtypes */ NULL );
        if ( !plan )
            raise_error( "SPI_prepare error" );

        Portal portal = SPI_cursor_open( /* name */ NULL, plan, /* values */ NULL, /* nulls */ NULL, /* read_only */ true );
        
        const TupleDesc& tuple_desc = portal->tupDesc;
        check_tuple_names_and_types( tuple_desc, {
                                                  {"arcs_id", "_int8"}, // array of int8
                                                  {"traffic_rules", "_int2"}, // array of int2
                                                  {"time_value", "int4"},
                                                  {"toll_rules", "_int2"}, // array of int2
                                                  {"toll_value", "float4"}
                                                 } 
                                   );
        
        size_t fetch_count = 1024;

        const int arc_seq_col_idx = SPI_fnumber( tuple_desc, "arcs_id" );
        const int traffic_rules_col_idx = SPI_fnumber( tuple_desc, "traffic_rules" );
        const int time_value_col_idx = SPI_fnumber( tuple_desc, "time_value" );
        const int toll_rules_col_idx = SPI_fnumber( tuple_desc, "toll_rules" );
        const int toll_value_col_idx = SPI_fnumber( tuple_desc, "toll_value" );
        
        graph->clear_restrictions();
        
        do {
            SPI_cursor_fetch( portal, /* forward */ true, fetch_count );
            for ( int row = 0; row < SPI_processed; row++ ) {
                Tempus::Restriction restriction;
                uint32_t traffic_rules;
                uint32_t toll_rules;
                bool isnull;
                bool error=false;
                Datum d;
                
                // read arc sequence
                {
                    ArrayType* array = DatumGetArrayTypeP( SPI_getbinval(SPI_tuptable->vals[row],
                                                                         SPI_tuptable->tupdesc,
                                                                         arc_seq_col_idx,
                                                                         &isnull) );
                    if ( isnull )
                        continue;

                    restriction.sequence = array1_to_vector<Tempus::db_id_t>( array );
                    
                    for ( auto& it = restriction.sequence.begin(); it!=restriction.sequence.end(); it++ ) {
                        Tempus::Optional<Tempus::Graph::edge_descriptor> arc = graph->arc_from_id( *it );
                        if ( ! arc ) {
                            elog( WARNING, "  Cannot find arc with ID = %lld in memory, skipping", *it );
                            error = true;
                        }
                    }                    
                    if ( error )
                        continue;  
                }
                
                // read traffic rules and time penalties
                d = SPI_getbinval(
                                 SPI_tuptable->vals[row],
                                 SPI_tuptable->tupdesc,
                                 traffic_rules_col_idx,
                                 &isnull
                             );             
                if (!isnull) {
                    ArrayType* array = DatumGetArrayTypeP( d );
                    traffic_rules = rules_vector_to_bitfield( array1_to_vector<uint16_t>( array ) );
                    d = SPI_getbinval(
                                         SPI_tuptable->vals[row],
                                         SPI_tuptable->tupdesc,
                                         time_value_col_idx,
                                         &isnull
                                     );
                    int time_value;
                    if (!isnull) time_value = DatumGetInt32( d ); 
                    else time_value = INT_MAX; 
                    restriction.time_penalty[traffic_rules] = time_value;
                }
                // read toll rules and fare penalties
                d = SPI_getbinval(
                                     SPI_tuptable->vals[row],
                                     SPI_tuptable->tupdesc,
                                     toll_rules_col_idx,
                                     &isnull
                                 );             
                if (!isnull) {
                    ArrayType* array = DatumGetArrayTypeP( d );
                    toll_rules = rules_vector_to_bitfield( array1_to_vector<uint16_t>( array ) );                        
                    // read toll value
                    d == SPI_getbinval(
                                          SPI_tuptable->vals[row],
                                          SPI_tuptable->tupdesc,
                                          toll_value_col_idx,
                                          &isnull
                                      ); 
                    float toll_value;
                    if ( !isnull ) toll_value = DatumGetFloat4( d ); 
                    else toll_value = FLT_MAX;
                    restriction.fare_penalty[toll_rules] = toll_value; 
                }
                graph->add_restriction( std::move( restriction ) );
            }
            
            SPI_freetuptable( SPI_tuptable );

        } while ( SPI_processed == fetch_count );

        SPI_cursor_close( portal );

        SPI_finish();

        graph->reset_restriction_automaton(); 
    }
    catch ( std::exception& e )
    {
        elog(ERROR, "%s", e.what());
    }
    
    PG_RETURN_VOID();
}

extern "C" Datum tempus_set_parking_costs(PG_FUNCTION_ARGS)
{
    using namespace Tempus; 
    try {
        if ( !PG_GETARG_POINTER(0) || PG_ARGISNULL(0) )
            raise_error( "A non null graph name is needed" );
        
        const char* graph_id = text_to_cstring(PG_GETARG_TEXT_P(0));
        auto reg_it = g_graph_registry.find( graph_id );
        if ( reg_it == g_graph_registry.end() ) {
            raise_error( "Graph %s not loaded !", graph_id );
        }
        if ( reg_it->second.graph->name() != "multimodal_graph" ) {
            raise_error( "Wrong graph type %s, expected: multimodal_graph", reg_it->second.graph->name().c_str() );
        }

        Tempus::Graph* graph = static_cast<Tempus::Graph*>( reg_it->second.graph.get() );

        if ( !PG_GETARG_POINTER(1) || PG_ARGISNULL(1) )
            raise_error( "A non null query is needed" );
        
        const char* query = text_to_cstring(PG_GETARG_TEXT_P(1));

        if ( SPI_connect() == SPI_ERROR_CONNECT )
            raise_error( "SPI_connect error" );

        SPIPlanPtr plan = SPI_prepare( query,
                                       /* nargs */ 0,
                                       /* argtypes */ NULL );
        if ( !plan )
            raise_error( "SPI_prepare error" );

        Portal portal = SPI_cursor_open( /* name */ NULL, plan, /* values */ NULL, /* nulls */ NULL, /* read_only */ true );

        const TupleDesc& tuple_desc = portal->tupDesc;
        check_tuple_names_and_types( tuple_desc, {
                                                  {"node_id", "int8"}, 
                                                  {"vehicle_parking_rule", "int2"},
                                                  {"leaving_time", "int4"},
                                                  {"leaving_fare", "float4"}, 
                                                  {"taking_time", "int4"},
                                                  {"taking_fare", "float4"}
            } );

        size_t fetch_count = 1024;
        
        const int node_col_idx = SPI_fnumber( tuple_desc, "node_id" );
        const int vehicle_parking_rule_col_idx = SPI_fnumber( tuple_desc, "vehicle_parking_rule" );
        const int leaving_time_col_idx = SPI_fnumber( tuple_desc, "leaving_time" );
        const int leaving_fare_col_idx = SPI_fnumber( tuple_desc, "leaving_fare" );
        const int taking_time_col_idx = SPI_fnumber( tuple_desc, "taking_time" );
        const int taking_fare_col_idx = SPI_fnumber( tuple_desc, "taking_fare" );
        
        graph->clear_parking_costs();
        
        do {
            SPI_cursor_fetch( portal, /* forward */ true, fetch_count );
            for ( int row = 0; row < SPI_processed; row++ ) {
                bool isnull;
                Tempus::GeneralizedCost leaving_cost;
                Tempus::GeneralizedCost taking_cost;
                
                // read node
                Tempus::db_id_t node_id = DatumGetInt64( SPI_getbinval(SPI_tuptable->vals[row],
                                                                        SPI_tuptable->tupdesc,
                                                                        node_col_idx,
                                                                        &isnull) );
                if ( isnull )
                    continue;
                
                // read vehicle parking rule
                uint32_t vehicle_parking_rule = DatumGetInt16( SPI_getbinval(SPI_tuptable->vals[row],
                                                                        SPI_tuptable->tupdesc,
                                                                        vehicle_parking_rule_col_idx,
                                                                        &isnull) );
                if ( isnull )
                    continue;
                
                // read leaving time
                int32_t leaving_time = DatumGetInt32( SPI_getbinval(SPI_tuptable->vals[row],
                                                            SPI_tuptable->tupdesc,
                                                            leaving_time_col_idx,
                                                            &isnull) );
                if ( isnull )
                    continue;

                // read leaving fare
                float leaving_fare = DatumGetInt32( SPI_getbinval(SPI_tuptable->vals[row],
                                                          SPI_tuptable->tupdesc,
                                                          leaving_fare_col_idx,
                                                          &isnull) );
                if ( isnull )
                    continue;
                
                // read taking time
                int32_t taking_time = DatumGetInt32( SPI_getbinval(SPI_tuptable->vals[row],
                                                            SPI_tuptable->tupdesc,
                                                            taking_time_col_idx,
                                                            &isnull) );
                if ( isnull )
                    continue;
                
                // read taking fare
                float taking_fare = DatumGetInt32( SPI_getbinval(SPI_tuptable->vals[row],
                                                          SPI_tuptable->tupdesc,
                                                          taking_fare_col_idx,
                                                          &isnull) );
                if ( isnull )
                    continue;

                leaving_cost.time = leaving_time;
                leaving_cost.fare = leaving_fare;
                
                taking_cost.time = taking_time;
                taking_cost.fare = taking_fare;
                
                Tempus::Optional<Tempus::Graph::vertex_descriptor> node = graph->node_from_id( node_id );
                if ( ! node ) {
                    elog( WARNING, "Cannot find node with ID = %lld in memory, skipping", node_id );
                    continue;
                }
                graph->add_parking_cost( *node, vehicle_parking_rule, leaving_cost, taking_cost);
            }

            SPI_freetuptable( SPI_tuptable );
            
        } while ( SPI_processed == fetch_count );

        SPI_cursor_close( portal );

        SPI_finish();
    }
    catch ( std::exception& e )
    {
        elog(ERROR, "%s", e.what());
    }
    
    PG_RETURN_VOID();
}


extern "C" Datum tempus_set_vehicles_positions(PG_FUNCTION_ARGS)
{
    using namespace Tempus; 
    try {
        if ( !PG_GETARG_POINTER(0) || PG_ARGISNULL(0) )
            raise_error( "A non null graph name is needed" );
        
        const char* graph_id = text_to_cstring(PG_GETARG_TEXT_P(0));
        auto reg_it = g_graph_registry.find( graph_id );
        if ( reg_it == g_graph_registry.end() ) {
            raise_error( "Graph %s not loaded !", graph_id );
        }
        if ( reg_it->second.graph->name() != "multimodal_graph" ) {
            raise_error( "Wrong graph type %s, expected: multimodal_graph", reg_it->second.graph->name().c_str() );
        }

        Tempus::Graph* graph = static_cast<Tempus::Graph*>( reg_it->second.graph.get() );
        
        if ( !PG_GETARG_POINTER(1) || PG_ARGISNULL(1) )
            raise_error( "A non null query is needed" );
        
        const char* query = text_to_cstring(PG_GETARG_TEXT_P(1));

        if ( SPI_connect() == SPI_ERROR_CONNECT )
            raise_error( "SPI_connect error" );

        SPIPlanPtr plan = SPI_prepare( query,
                                       /* nargs */ 0,
                                       /* argtypes */ NULL );
        if ( !plan )
            raise_error( "SPI_prepare error" );

        Portal portal = SPI_cursor_open( /* name */ NULL, plan, /* values */ NULL, /* nulls */ NULL, /* read_only */ true );

        const TupleDesc& tuple_desc = portal->tupDesc;
        check_tuple_names_and_types( tuple_desc, {
                                                  {"node_id", "int8"}, 
                                                  {"transport_mode_id", "int2"}
            } );

        size_t fetch_count = 1024;
        
        const int node_col_idx = SPI_fnumber( tuple_desc, "node_id" );
        const int transport_mode_col_idx = SPI_fnumber( tuple_desc, "transport_mode_id" );
        
        graph->clear_vehicles();
        
        do {
            SPI_cursor_fetch( portal, /* forward */ true, fetch_count );
            for ( int row = 0; row < SPI_processed; row++ ) {
                bool isnull;
                
                // read node
                Tempus::db_id_t node_id = DatumGetInt64( SPI_getbinval(SPI_tuptable->vals[row],
                                                                        SPI_tuptable->tupdesc,
                                                                        node_col_idx,
                                                                        &isnull) );
                if ( isnull )
                    continue;
                
                // read vehicle parking rule
                uint32_t transport_mode_id = DatumGetInt16( SPI_getbinval(SPI_tuptable->vals[row],
                                                                        SPI_tuptable->tupdesc,
                                                                        transport_mode_col_idx,
                                                                        &isnull) );
                if ( isnull )
                    continue;
                
                Tempus::Optional<Tempus::Graph::vertex_descriptor> node = graph->node_from_id( node_id );
                if ( ! node ) {
                    elog( WARNING, "Cannot find node with ID = %lld in memory, skipping", node_id );
                    continue;
                }
                graph->add_vehicle( *node, transport_mode_id );
            }

            SPI_freetuptable( SPI_tuptable );
            
        } while ( SPI_processed == fetch_count );

        SPI_cursor_close( portal );

        SPI_finish();
    }
    catch ( std::exception& e )
    {
        elog(ERROR, "%s", e.what());
    }
    
    PG_RETURN_VOID();
}


extern "C" Datum tempus_set_pt_boarding_security_times(PG_FUNCTION_ARGS)
{
    using namespace Tempus;
    
    try {
        // read graph
        if ( !PG_GETARG_POINTER(0) || PG_ARGISNULL(0) )
            raise_error( "A non null graph name is needed" );
        
        const char* graph_id = text_to_cstring(PG_GETARG_TEXT_P(0));
        
        auto reg_it = g_graph_registry.find( graph_id );
        if ( reg_it == g_graph_registry.end() ) {
            raise_error( "Graph %s not loaded !", graph_id );
        }
        if ( reg_it->second.graph->name() != "multimodal_graph" ) {
            raise_error( "Wrong graph type %s, expected: multimodal_graph", reg_it->second.graph->name().c_str() );
        }

        Tempus::Graph* graph = static_cast<Tempus::Graph*>( reg_it->second.graph.get() );

        // read query
        if ( !PG_GETARG_POINTER(1) || PG_ARGISNULL(1) )
            raise_error( "A non null query is needed" );
        
        const char* query = text_to_cstring(PG_GETARG_TEXT_P(1));
        
        if ( SPI_connect() == SPI_ERROR_CONNECT )
            raise_error( "SPI_connect error" );

        SPIPlanPtr plan = SPI_prepare( query,
                                       /* nargs */ 0,
                                       /* argtypes */ NULL );
        if ( !plan )
            raise_error( "SPI_prepare error" );

        Portal portal = SPI_cursor_open( /* name */ NULL, plan, /* values */ NULL, /* nulls */ NULL, /* read_only */ true );

        const TupleDesc& tuple_desc = portal->tupDesc;
        check_tuple_names_and_types( tuple_desc, {
                                                  {"node_id", "int8"}, 
                                                  {"pt_boarding_security_time", "int2"}
            } );
        
        size_t fetch_count = 1024;
        
        const int node_col_idx = SPI_fnumber( tuple_desc, "node_id" );
        const int boarding_security_time_col_idx = SPI_fnumber( tuple_desc, "pt_boarding_security_time" );
        
        graph->clear_pt_boarding_security_times();
        
        do {
            SPI_cursor_fetch( portal, /* forward */ true, fetch_count );
            for ( int row = 0; row < SPI_processed; row++ ) {
                bool isnull;
                
                Tempus::db_id_t node_id = DatumGetInt64( SPI_getbinval(SPI_tuptable->vals[row],
                                                                        SPI_tuptable->tupdesc,
                                                                        node_col_idx,
                                                                        &isnull) );
                if ( isnull )
                    continue;
                
                uint32_t boarding_security_time = DatumGetInt16( SPI_getbinval(SPI_tuptable->vals[row],
                                                                                SPI_tuptable->tupdesc,
                                                                                boarding_security_time_col_idx,
                                                                                &isnull) );
                if ( isnull )
                    continue;

                Tempus::Optional<Tempus::Graph::vertex_descriptor> node = graph->node_from_id( node_id );
                if ( ! node ) {
                    elog( WARNING, "Cannot find node with ID = %lld in memory, skipping", node_id );
                    continue;
                }
                graph->update_pt_boarding_security_time( *node, boarding_security_time );
            }
            
            SPI_freetuptable( SPI_tuptable );
        
        } while ( SPI_processed == fetch_count );

        SPI_cursor_close( portal );

        SPI_finish();
    }
    catch ( std::exception& e )
    {
        elog(ERROR, "%s", e.what());
    }
    
    PG_RETURN_VOID();
}
