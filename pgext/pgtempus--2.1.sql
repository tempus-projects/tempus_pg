-- complain if script is sourced in psql, rather than via CREATE EXTENSION
\echo Use "CREATE EXTENSION pgtempus" to load this file. \quit

--
-- Convenience function that return the current db connection (if no password)
--
create or replace function tempus_current_db_connection()
returns text
language sql
as $$
  select format('host=%s port=%s dbname=%s user=%s',
         case when inet_server_addr()::text = '::1/128' then 'localhost'
              when inet_server_addr() is null then 'localhost'
              else inet_server_addr()::text end,
         (select setting from pg_settings where name='port'),
         current_database(),
         current_user);
$$;


--
-- Loaded graphs view
create table session_graph(
  id text,
  dbstring text,
  graph_type text,
  options text
);

--create or replace function tempus_build_graph(id text, dbstring text, graph_type text default null, options hstore default null) returns void
--as '$libdir/pgtempus', 'tempus_build_graph'
--language C volatile;

-- Graph management functions
create or replace function tempus_build_multimodal_graph(id text, transport_modes_query text, nodes_query text, arcs_query text) 
returns void
as '$libdir/pgtempus', 'tempus_build_multimodal_graph'
language C volatile;

create or replace function tempus_loaded_graphs(id out text, dbstring out text, graph_type out text, options out text) 
returns setof record
as '$libdir/pgtempus', 'tempus_loaded_graphs'
language C volatile;

create or replace function tempus_unload_graph(id text) 
returns void
as '$libdir/pgtempus', 'tempus_unload_graph'
language C volatile;

-- SELECT on the view
create rule "_RETURN" as on select to session_graph do instead select * from tempus_loaded_graphs();
-- INSERT rule => build_graph
--create rule insert_session_graph_rule as on insert to session_graph do instead select tempus_build_graph(NEW.id, NEW.dbstring, NEW.graph_type, NEW.options);
create rule insert_session_graph_rule as on insert to session_graph do instead nothing;
-- DELETE rule => unload_graph
create rule delete_session_graph_rule as on delete to session_graph do instead select tempus_unload_graph(OLD.id);
-- UPDATE rule  => nothing
create rule update_session_graph_rule as on update to session_graph do instead nothing;

-- Costs functions
create or replace function tempus_set_static_road_section_costs(graph_id text, query text)
returns void
as '$libdir/pgtempus', 'tempus_set_static_road_section_costs'
language C volatile;

create or replace function tempus_set_pt_section_timetable(graph_id text, query text, time_origin timestamp)
returns void
as '$libdir/pgtempus', 'tempus_set_pt_section_timetable'
language C volatile;

create or replace function tempus_set_arcs_sequence_costs(graph_id text, query text)
returns void
as '$libdir/pgtempus', 'tempus_set_arcs_sequence_costs'
language C volatile;

create or replace function tempus_set_parking_costs(graph_id text, query text)
returns void
as '$libdir/pgtempus', 'tempus_set_parking_costs'
language C volatile;

create or replace function tempus_set_pt_boarding_security_times(graph_id text, query text)
returns void
as '$libdir/pgtempus', 'tempus_set_pt_boarding_security_times'
language C volatile;

create or replace function tempus_set_vehicles_positions(graph_id text, query text)
returns void
as '$libdir/pgtempus', 'tempus_set_vehicles_positions'
language C volatile;

-- Shortest paths functions
create function tempus_one_to_many_paths (
                                            graph_id text,
                                            algo_name text,
                                            orig_node bigint,
                                            dest_nodes bigint[],
                                            orig_transport_mode smallint,
                                            dest_transport_mode smallint,
                                            max_cost float4,
                                            allowed_transport_modes bigint[],
                                            departure_time timestamp without time zone, 
                                            pt_time_weight real,
                                            waiting_time_weight real,
                                            indiv_mode_time_weight real,
                                            fare_weight real,
                                            pt_transfer_weight real,
                                            mode_change_weight real,
                                            max_walking_time interval,
                                            max_bicycle_time interval,
                                            max_pt_transfers smallint,
                                            max_mode_changes smallint
                                         ) returns table (
                                                            obj_id bigint,
                                                            pred_obj_id bigint,
                                                            arc_id bigint,
                                                            node_id bigint,
                                                            pred_node_id bigint,
                                                            transport_mode_id smallint,
                                                            automaton_state integer,
                                                            cost real, 
                                                            pred_cost real,
                                                            pt_trip_id bigint,
                                                            waiting_time interval,
                                                            departure_time interval,
                                                            arrival_time interval,
                                                            total_time interval,
                                                            total_walking_time interval,
                                                            total_bicycle_time interval,
                                                            total_pt_transfers smallint,
                                                            total_mode_changes smallint, 
                                                            total_fare real,
                                                            in_paths boolean
                                                         )
as '$libdir/pgtempus', 'tempus_one_to_many_paths'
language C volatile;



create function tempus_many_to_one_paths (
                                            graph_id text,
                                            algo_name text,
                                            dest_node bigint,
                                            orig_nodes bigint[],
                                            dest_transport_mode smallint,
                                            orig_transport_mode smallint,
                                            max_cost float4,
                                            allowed_transport_modes bigint[],
                                            arrival_time timestamp without time zone, 
                                            pt_time_weight real,
                                            waiting_time_weight real, 
                                            indiv_mode_time_weight real,
                                            fare_weight real,
                                            pt_transfer_weight real,
                                            mode_change_weight real,
                                            max_walking_time interval,
                                            max_bicycle_time interval,
                                            max_pt_transfers smallint, 
                                            max_mode_changes smallint
                                          ) returns table (
                                                            obj_id bigint,
                                                            pred_obj_id bigint,
                                                            arc_id bigint,
                                                            node_id bigint,
                                                            pred_node_id bigint,
                                                            transport_mode_id smallint,
                                                            automaton_state integer,
                                                            cost real, 
                                                            pred_cost real,
                                                            pt_trip_id bigint,
                                                            waiting_time interval,
                                                            departure_time interval,
                                                            arrival_time interval,
                                                            total_time interval,
                                                            total_walking_time interval,
                                                            total_bicycle_time interval,
                                                            total_pt_transfers smallint,
                                                            total_mode_changes smallint, 
                                                            total_fare real, 
                                                            in_paths boolean
                                                          )
as '$libdir/pgtempus', 'tempus_many_to_one_paths'
language C volatile;

