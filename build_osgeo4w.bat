cmake -G "NMake Makefiles" ^
  -DPG_CONFIG=C:\osgeo4w64\bin\pg_config.exe ^
  -DCMAKE_BUILD_TYPE=RelWithDebInfo ^
  -DCMAKE_INSTALL_PREFIX=C:\osgeo4w64\ ^
  -DBoost_INCLUDE_DIR=C:\osgeo4w64\include\boost-1_63 ^
  -DBoost_COMPILER=-vc140 ^
  -DTEMPUS_INCLUDE_DIRECTORY=C:\osgeo4w64\include ^
  ..
nmake