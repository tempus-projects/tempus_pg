create table test.test1_nodes (
  id int8
  , parking_transport_modes int2
  , x float4
  , y float4
  , z float4
);

insert into test.test1_nodes values
  (1, 0, 0.0, 0.0, 0.0),
  (2, 0, 1.0, 0.0, 0.0),
  (3, 0, 0.0, 1.0, 0.0),
  (4, 0, 1.0, 1.0, 0.0)
;

create table test.test1_edges (
  id int8
  , node_from_id int8
  , node_to_id int8
  , is_pt boolean
  , traffic_rules int2[]
);

insert into test.test1_edges values
  (1, 1, 2, 'f', '{1}'),
  (2, 2, 3, 'f', '{1}'),
  -- an edge with no traffic possible
  (3, 1, 3, 'f', null),
  -- 
  (4, 3, 4, 'f', '{1}')
;

select tempus_build_multimodal_graph('g', $sql$
  select 1::int8 as id
  , 'Walking'::text as name
  , 'Walking'::text as category
  , '{1}'::int2[] as traffic_rules
$sql$,
$sql$
  select * from test.test1_nodes
$sql$,
$sql$
  select * from test.test1_edges
$sql$
);

select is((select count(*) from session_graph)::integer, 1, 'New graph g is created');
select is((select id from session_graph), 'g', 'New graph name');
select is((select graph_type from session_graph), 'multimodal_graph', 'New graph type');

-- unknown node
select throws_ok('select test.one_to_many_node_paths( ''g'', 1, array[10], 1 )');
select throws_ok('select test.one_to_many_node_paths( ''g'', 10, array[10], 1 )');
-- unknown transport mode
select throws_ok('select test.one_to_many_node_paths( ''g'', 1, array[2], 10 )');

select test.one_to_many_node_paths('g', 1, '{4}', '{1}', null, 'No path before setting costs');

-- set constant road costs
select tempus_set_static_road_edge_cost('g', $sql$
  select
    e.id as edge_id,
    1::int4 as mode_id,
    10::int4 as duration,
    0.::float4 as additional_cost
  from
    test.test1_edges e
$sql$,
1.0 -- time cost weight
);

select test.one_to_many_node_paths('g', 1, array[4], '{1}', '{"{1,2,3,4} - 30 - 00:00:30"}', 'Path from node 1 to 4');
select test.one_to_many_node_paths('g', 4, array[1], '{1}', null, 'No path from node 4 to 1');

-- reverse path
select test.many_to_one_node_paths('g', 4, '{1}', '{1}', '2000-1-1 12:00:00', '{"{1,2,3,4} - 43170 - 11:59:30"}', 'Path from 1 to 4, before 12:00');

-- Add some PT edges

insert into test.test1_nodes (id)
values (5), (6), (7),
(8), (9);

insert into test.test1_edges (id, node_from_id, node_to_id, is_pt, traffic_rules) values
  (5, 3, 5, 't', null)
, (6, 5, 6, 't', null)
, (7, 6, 7, 't', null)
-- road between 3 and 7
, (8, 3, 7, 'f', '{1}')
, (9, 8, 9, 'f', '{1}')
, (10, 9, 3, 'f', '{1}')
;
        
-- build a new graph
select tempus_build_multimodal_graph('g_pt', $sql$
  select 1::int8 as id
  , 'Walking'::text as name
  , 'Walking'::text as category
  , '{1}'::int2[] as traffic_rules
  union all
  select 2::int8 as id
  , 'PT'::text as name
  , 'Public transport'::text as category
  , null as traffic_rules
$sql$,
$sql$
  select * from test.test1_nodes
$sql$,
$sql$
  select * from test.test1_edges
$sql$
);

select test.one_to_many_node_paths('g_pt', 1, '{7}', '{1}', null, 'No path from node 1 to 7 without PT costs');

-- set constant road costs
select tempus_set_static_road_edge_cost('g_pt', $sql$
  select
    e.id as edge_id,
    1::int4 as mode_id,
    10::int4 as duration,
    0.::float4 as additional_cost
  from
    test.test1_edges e
  where not e.is_pt
$sql$,
1.0 -- time cost weight
);

-- set PT costs
select tempus_set_pt_edge_timetable('g_pt', $sql$
  select * from (values
    (5::int8, '10:00'::interval, '10:30'::interval, 1::int8),
    (6::int8, '10:30'::interval, '10:50'::interval, 1::int8),
    (7::int8, '10:50'::interval, '11:00'::interval, 1::int8)
  ) as t(edge_id, departure, arrival, trip_id)
$sql$,
'2000-01-01'
);

select test.one_to_many_node_paths('g_pt', 3, '{7}', '{2}', '{"{3,5,6,7} - 39600 - 11:00:00"}', 'Path from 3 to 7 with only PT mode');

select test.one_to_many_node_paths('g_pt', 3, '{7}', '{1,2}', '{"{3,7} - 10 - 00:00:10","{3,5,6,7} - 39600 - 11:00:00"}', 'Path from 3 to 7 with two modes');

-- multi destinations
select test.one_to_many_node_paths('g_pt', 1, '{4,7}', '{1}', '{"{1,2,3,4} - 30 - 00:00:30","{1,2,3,7} - 30 - 00:00:30"}', 'Paths with multiple destinations');


-- test many to one paths
select test.many_to_one_node_paths('g_pt', 7, '{3}', '{2}', '2000-1-1 12:00:00', '{"{3,5,6,7} - 36000 - 10:00:00"}', 'Path from 3 to 7 with only PT mode, before 12:00');

select test.many_to_one_node_paths('g_pt', 7, '{3}', '{1, 2}', '2000-1-1 12:00:00', '{"{3,7} - 43190 - 11:59:50","{3,5,6,7} - 36000 - 10:00:00"}', 'Path from 3 to 7 with two modes, before 12:00');

select test.many_to_one_node_paths('g_pt', 7, '{1,8}', '{1}', '2000-1-1 12:00:00', '{"{1,2,3,7} - 43170 - 11:59:30","{8,9,3,7} - 43170 - 11:59:30"}', 'Paths with multiple destinations, before 12:00');





