pgTempus
========

pgTempus is an extension for PostgreSQL that allows the resolution of shortest path planning requests from the database.

Installation
------------

Then you have to call `CREATE EXTENSION pgtempus;` on a database where you want this extension to be enabled.
Note that you can use the schema relocation ability of PostgreSQL to install the extension into its own schema:
```
CREATE EXTENSION pgtempus SCHEMA pgtempus;
```

Functions reference
===================

## Debugging

A custom parameter allows the user to enable or disable debugging information when running Tempus functions.

```SQL
set pgtempus.debug = on;
```

## In-memory graph management

One of the key feature of pgTempus is that it **keeps graphs in memory** during the duration of the current session.

It allows the reuse of in-memory graphs for each request, rather than having to reload the graph in memory before each request.

### `tempus_build_multimodal_graph(id text, transport_modes_query text, nodes_query text, edges_query text)`

Parameters:

- `id` : since graphs are kept in memory, they must be refered to by a unique name. The `id` parameter allows the caller to specify a graph identifier.
- `transport_modes_query` : an SQL query that will return a table describing transport modes. This table must at least contain the following columns:
  - `id int8` : the unique identifier of the transport mode
  - `name text` (not used for now) : the transport mode name
  - `category text`: the transport mode category, can be 'Walking', 'Private vehicle', 'Public transport' or 'Shared vehicle service'
  - `traffic_rules int2[]` : an array of enabled traffic rules for this transport mode. The interpretation is kept free to the caller but must be linked to the traffic rules of an edge (see below). Since it is represented internally by a bitfield, **identifiers of traffic rules must be > 0 and <= 32**.
- `nodes_query`: an SQL query that returns a table describing nodes of the graph. This table must at least contain the following columns:
  - `id int8` : a unique identifier
  - `parking_transport_modes int2` (not used for now) : a bitfield of transport modes that can park on this node
  - `x float4` (not used for now) : the x coordinate of the node
  - `y float4` (not used for now) : the y coordinate of the node
  - `z float4` (not used for now) : the z coordinate of the node
- `edges_query` : an SQL query that returns a table describing edges of the graph. This table must at least contain the following columns:
  - `id int8` : unique identifier of the edge
  - `node_from_id int8` : node identifier of the first node of the edge. This must refers an identifier that exists in the node table.
  - `node_to_id int8` : node identifier of the second node of the edge. This must refers an identifier that exists in the node table.
  - `is_pt bool` : whether the edge is reserved to a public transport network
  - `traffic_rules int2[]` : an array of enabled traffic rules id for this edge.
  
### `tempus_loaded_graphs`

Returns all the loaded graphs that are currently kept in memory.

### `tempus_unload_graph(id text)`

Unloads a graph from memory, given its unique identifier.

**Note**: all graphs will be automatically unloaded at the end of a session.
  
### The `session_graph` view

The `session_graph` view reflects the current state of in-memory graphs.

SELECT on this view is equivalent to calling `tempus_loaded_graphs()`.

DELETE on this view is equivalent to calling `tempus_unload_graph()` for a given graph id.

## Generalized costs

(Static) generalized costs can be set on a set of edges by calling the given function:

`tempus_set_static_road_edge_cost(graph_id text, query text, time_cost_weight float4)`

Parameters:
- `graph_id` the identifier of the graph on which costs will be set
- `query text` : an SQL query that should return a table with the following columns:
  - `edge_id int8` : identifier of the edge on which to set the cost
  - `mode_id int4` : transport mode id
  - `duration int4` : time cost, in seconds
  - `additional_cost float4` : additional cost
- `time_cost_weight` : how to weight the time cost for the overall generalized cost.

The generalized cost for a given edge and transport mode is a floating point number given by:

- `duration` x `time_cost_weight` + additional_cost

## Public transport costs

Edges can have a timetable associated that will set a cost that is a function of time.

`tempus_set_pt_edge_timetable(graph_id text, query text, time_cost_origin timestamp)`

Parameters:
- `graph_id` the identifier of the graph on which costs will be set
- `query text` : an SQL query that should return a table with the following columns:
  - `edge_id int8` : identifier of the edge on which to set the cost
  - `departure interval` : departure time, set from the `time_cost_origin`
  - `arrival interval` : arrival time, set from the `time_cost_origin`
- `time_cost_origin` : time origin

## Cost restrictions

Cost restrictions can be set on sequences of edges for a given transport mode.
This mechanism allows the expression of turn restrictions or more general restrictions.

`tempus_set_cost_restrictions(graph_id text, query text)`

Parameters :
- `graph_id` the identifier of the graph on which cost restrictions will be set
- `query text` : an SQL query that should return a table with the following columns:
  - `edge_sequence int8[]` : an array that represents a sequence of edges
  - `mode_id int8` : the transport mode on which the restriction must be set
  - `time int4` : time penalty, in seconds
  - `cost float4` : additional cost penaly


## Route planning

The following function allows the computation of :
- the best route between an origin node and a set of destinations
- or an *isochrone* from an origin node until a maximum cost is reached

Function signature :

```SQL
create function tempus_one_to_many_paths (
       graph_id text,
       algo_name text,
       start_node bigint,
       end_nodes bigint[],
       max_cost float4,
       dep_time timestamp without time zone,
       allowed_mode_ids bigint[]
) returns setof roadmap
```

Parameters:

- `graph_id` : the graph id on which to compute the route
- `algo_name` : name of the algorithm. Only `multimodal` is supported for now.
- `start_node` : id of the starting node
- `end_nodes` : an array of ending nodes' id
- `max_cost` : the maximum (generalized) cost for the isochrone. Set to `null` if no isochrone is needed
- `dep_time` : the departure time
- `allowed_mode_ids` : array of tranport mode IDs that are allowed

The function will return a set of rows conforming to the following structure:

```SQL
create type roadmap as (
    id int4,
    predecessor_id int4,
    edge_id bigint,
    node_from_id int8,
    node_to_id int8,
    accumulated_cost float4,
    transport_mode_id bigint,
    pt_trip_id bigint,
    wait_time interval,
    departure_time interval,
    arrival_time interval
);
```

A set of **steps** are returned, with the following description:

- `id` : a unique identifier of the step
- `predecessor_id` : a link to the predecessor step. Each path can then be recomposed by starting at the step carrying the end node and browsing the returned set by following `predecessor_id`
- `edge_id` : the edge of this step
- `node_from_id` : id of the first node of the edge. Just for convenience to avoid an additional join on the edge table
- `node_to_id` : id of the first node of the edge. Just for convenience to avoid an additional join on the edge table
- `accumulated_cost` : cost accumulated so far from the origin
- `transport_mode_id` : transport mode taken on the current step
- `pt_trip_id` : public transport trip id, if any (only meaningful for public transports)
- `wait_time` : time to wait before departing (only meaningful for public transports)
- `departure_time` : departure time (after the wait time)
- `arrival_time` : arrival_time at the end of the step


## "Arrive before" requests

Routes where a latest time of arrival must be respected can be requested through the `tempus_many_to_one_paths` function.
It works by exploring the graph in backward: by starting from the arrival node and going through in-edges.
The restriction cost automaton is also reversed in the same way.
If a node has a timetable defined, the search for a trip is done by looking at the previous arrival, rather than looking at the next departure.

The function signature is similar to the signature for regular "one-to-many" paths :

```SQL
create function tempus_many_to_one_paths (
       graph_id text,
       algo_name text,
       end_node bigint,
       start_nodes bigint[],
       max_cost float4,
       arrival_time timestamp without time zone,
       allowed_mode_ids bigint[]
) returns setof roadmap
```

And the type of result is also similar.

Examples
========

Assuming we have the following data tables:

```SQL
create table test1_nodes (
  id int8
  , parking_transport_modes int2
  , x float4
  , y float4
  , z float4
);

insert into test1_nodes (id)
select * from generate_series(1,7);

create table test1_edges (
  id int8
  , node_from_id int8
  , node_to_id int8
  , is_pt boolean
  , traffic_rules int2[]
);

insert into test1_edges values
  (1, 1, 2, 'f', '{1}'),
  (2, 2, 3, 'f', '{1}'),
  -- an edge with no traffic possible
  (3, 1, 3, 'f', null),
  -- 
  (4, 3, 4, 'f', '{1}'),
  (5, 3, 5, 't', null),
  (6, 5, 6, 't', null),
  (7, 6, 7, 't', null),
-- road between 3 and 7
  (8, 3, 7, 'f', '{1}')
;

-- build a multimodal graph
-- We create two transport modes:
-- 1 = Walking
-- 2 = A public transport
select tempus_build_multimodal_graph('g', $sql$
  select 1::int8 as id
  , 'Walking'::text as name
  , 'Walking'::text as category
  , '{1}'::int2[] as traffic_rules
  union all
  select 2::int8 as id
  , 'PT'::text as name
  , 'Public transport'::text as category
  , null::int2[] as traffic_rules
$sql$,
$sql$
  select * from test1_nodes
$sql$,
$sql$
  select * from test1_edges
$sql$
);
```

Example of a session that shows the manipulation of a multimodal graph:


```
# select * from session_graph;
 id | dbstring |    graph_type    | options 
----+----------+------------------+---------
 g  |          | multimodal_graph | 

# select * from tempus_one_to_many_paths('g', 'multimodal',1, '{4}', null, null, '{1}');
 id | predecessor_id | edge_id | node_from_id | node_to_id | accumulated_cost | transport_mode_id | pt_trip_id | wait_time | departure_time | arrival_time 
----+----------------+---------+--------------+------------+------------------+-------------------+------------+-----------+----------------+--------------
```

No row returned ! This is normal, since no costs have been defined on edges. Let's set some static costs:

```SQL
# -- set constant costs
select tempus_set_static_road_edge_cost('g', $sql$
  select
    e.id as edge_id,
    1::int4 as mode_id,
    10::int4 as duration,
    0.::float4 as additional_cost
  from
    test1_edges e
$sql$,
1.0 -- time cost weight
);

# select * from tempus_one_to_many_paths('g', 'multimodal',1, '{4}', null, null, '{1}');
 id | predecessor_id | edge_id | node_from_id | node_to_id | accumulated_cost | transport_mode_id | pt_trip_id | wait_time | departure_time | arrival_time 
----+----------------+---------+--------------+------------+------------------+-------------------+------------+-----------+----------------+--------------
  4 |              3 |       4 |            3 |          4 |               30 |                 1 |            | 00:00:00  | 00:00:20       | 00:00:30
  3 |              2 |       2 |            2 |          3 |               20 |                 1 |            | 00:00:00  | 00:00:10       | 00:00:20
  2 |              1 |       1 |            1 |          2 |               10 |                 1 |            | 00:00:00  | 00:00:00       | 00:00:10
  1 |              0 |       0 |            0 |          0 |                0 |                 1 |            | 00:00:00  | 00:00:00       | 00:00:00

```

From the table returned we can follow the chain of `id` and `predecessor_id` to build the best path that goes through nodes 1, 2, 3 and 4.

Now, let's set some timetable for the public transport edges. We set times from a time origin also set to 2019-11-25 at midnight.

```SQL
select tempus_set_pt_edge_timetable('g', $sql$
  select * from (values
    (5::int8, '10:00'::interval, '10:30'::interval, 1::int8),
    (6::int8, '10:30'::interval, '10:50'::interval, 1::int8),
    (7::int8, '10:50'::interval, '11:00'::interval, 1::int8)
  ) as t(edge_id, departure, arrival, trip_id)
$sql$,
'2019-11-25'
);
```

Now, for a path from the node 3 to 7, only taking the public transport and departing at 10:30:00.

```
# select * from tempus_one_to_many_paths('g', 'multimodal',3, '{7}', null, '2019-11-25 09:30:00', '{2}');
 id | predecessor_id | edge_id | node_from_id | node_to_id | accumulated_cost | transport_mode_id | pt_trip_id | wait_time | departure_time | arrival_time 
----+----------------+---------+--------------+------------+------------------+-------------------+------------+-----------+----------------+--------------
  5 |              4 |       7 |            6 |          7 |            39600 |                 2 |          1 | 00:00:00  | 10:50:00       | 11:00:00
  4 |              3 |       6 |            5 |          6 |            39000 |                 2 |          1 | 00:00:00  | 10:30:00       | 10:50:00
  3 |              1 |       5 |            3 |          5 |            37800 |                 2 |          1 | 00:30:00  | 10:00:00       | 10:30:00
  1 |              0 |       0 |            0 |          0 |            34200 |                 2 |            | 00:00:00  | 00:00:00       | 09:30:00
```

We can notice we need to wait 30 minutes on node 3 before taking the public transport trip.

We can also ask for a multimodal path, allowing the two modes (walking: 1 and public transport: 2).

```
# select * from tempus_one_to_many_paths('g', 'multimodal',3, '{7}', null, '2019-11-25 09:30:00', '{1,2}');
 id | predecessor_id | edge_id | node_from_id | node_to_id | accumulated_cost | transport_mode_id | pt_trip_id | wait_time | departure_time | arrival_time 
----+----------------+---------+--------------+------------+------------------+-------------------+------------+-----------+----------------+--------------
  7 |              1 |       8 |            3 |          7 |            34210 |                 1 |            | 00:00:00  | 09:30:00       | 09:30:10
  1 |              0 |       0 |            0 |          0 |            34200 |                 1 |            | 00:00:00  | 00:00:00       | 09:30:00
  8 |              6 |       7 |            6 |          7 |            39600 |                 2 |          1 | 00:00:00  | 10:50:00       | 11:00:00
  6 |              5 |       6 |            5 |          6 |            39000 |                 2 |          1 | 00:00:00  | 10:30:00       | 10:50:00
  5 |              1 |       5 |            3 |          5 |            37800 |                 2 |          1 | 00:30:00  | 10:00:00       | 10:30:00
```

The result contains two paths.

We can start by the row with `id = 7` that corresponds to the arrival on node 7 by walk (`transport_mode_id = 1`).
By following rows by `predecessor_id`, we can reconstruct the path from 3 to 7 which is the direct edge from 3 to 7.

The second path starts by the row with `id = 8` for the arrival on node 7 by public transport. The resulting path is 3, 5, 6, 7.

Starting from the first node, we can also ask paths for two different destinations (node 4 and node 7) by walk.

```
# select * from tempus_one_to_many_paths('g', 'multimodal',1, '{4,7}', null, '2019-11-25 09:30:00', '{1}');
 id | predecessor_id | edge_id | node_from_id | node_to_id | accumulated_cost | transport_mode_id | pt_trip_id | wait_time | departure_time | arrival_time 
----+----------------+---------+--------------+------------+------------------+-------------------+------------+-----------+----------------+--------------
  4 |              3 |       4 |            3 |          4 |            34230 |                 1 |            | 00:00:00  | 09:30:20       | 09:30:30
  3 |              2 |       2 |            2 |          3 |            34220 |                 1 |            | 00:00:00  | 09:30:10       | 09:30:20
  2 |              1 |       1 |            1 |          2 |            34210 |                 1 |            | 00:00:00  | 09:30:00       | 09:30:10
  1 |              0 |       0 |            0 |          0 |            34200 |                 1 |            | 00:00:00  | 00:00:00       | 09:30:00
  5 |              3 |       8 |            3 |          7 |            34230 |                 1 |            | 00:00:00  | 09:30:20       | 09:30:30
```

From the result, we can reconstruct two different paths: 1,2,3,4 and 1,2,3,7.

Cost restriction example
========================

Suppose the following graph 'r' with a static cost of 10 on each edge (edge number between parenthesis):

```
  (4)
4<-----3
|      ^
|(5)   |
|      |(2)
V (3)  |
5<-----2
|      ^
|      |
|(6)   |(1)
V      |
6      1
```

Going from 1 to 6 gives the path 1,2,5,6 where the "shortcut" 2-5 is taken:

```
# select * from tempus_one_to_many_paths('r', 'multimodal',1, '{6}', null, null, '{1}');
 id | predecessor_id | edge_id | node_from_id | node_to_id | accumulated_cost | transport_mode_id | pt_trip_id | wait_time | departure_time | arrival_time 
----+----------------+---------+--------------+------------+------------------+-------------------+------------+-----------+----------------+--------------
  6 |              5 |       6 |            5 |          6 |               30 |                 1 |            | 00:00:00  | 00:00:20       | 00:00:30
  5 |              2 |       3 |            2 |          5 |               20 |                 1 |            | 00:00:00  | 00:00:10       | 00:00:20
  2 |              1 |       1 |            1 |          2 |               10 |                 1 |            | 00:00:00  | 00:00:00       | 00:00:10
  1 |              0 |       0 |            0 |          0 |                0 |                 1 |            | 00:00:00  | 00:00:00       | 00:00:00
```

Now setting a cost penalty of 40 on the sequence of edges 1,3,6 (= nodes 1,2,5,6) gives a different result:
```
# select tempus_set_cost_restrictions('r',
$sql$
  select '{1,3,6}'::int8[] as edge_sequence, 1::int8 as mode_id, 40::int4 as time, .0::float4 as cost
$sql$
);
# select * from tempus_one_to_many_paths('r', 'multimodal',1, '{6}', null, null, '{1}');
 id | predecessor_id | edge_id | node_from_id | node_to_id | accumulated_cost | transport_mode_id | pt_trip_id | wait_time | departure_time | arrival_time 
----+----------------+---------+--------------+------------+------------------+-------------------+------------+-----------+----------------+--------------
  8 |              6 |       6 |            5 |          6 |               50 |                 1 |            | 00:00:00  | 00:00:40       | 00:00:50
  6 |              5 |       5 |            4 |          5 |               40 |                 1 |            | 00:00:00  | 00:00:30       | 00:00:40
  5 |              4 |       4 |            3 |          4 |               30 |                 1 |            | 00:00:00  | 00:00:20       | 00:00:30
  4 |              3 |       2 |            2 |          3 |               20 |                 1 |            | 00:00:00  | 00:00:10       | 00:00:20
  3 |              1 |       1 |            1 |          2 |               10 |                 1 |            | 00:00:00  | 00:00:00       | 00:00:10
  1 |              0 |       0 |            0 |          0 |                0 |                 1 |            | 00:00:00  | 00:00:00       | 00:00:00
```

The resulting path is now 1,2,3,4,5,6, where the "shortcut" 2-5 is not part of the best path.

Isochrone example
=================

To illustrate the isochrone request, have a look at the corresponding [test](tests/test_isochrones.sql) where a "circular" graph is built around a center and 3 different levels of nodes. We test that only 2 levels of nodes are returned if we set the appropriate maximum cost to reach.

Tests
=====

The API is tested for regressions using [pgTap](https://pgtap.org/). It is included in the sources and the binary part of pgtap is not needed. There is then no extra dependency to install in order to test the extension.

The main SQL script [tests.sql](tests/tests.sql) will run tests. Everything is run in a transaction that will be rolled back so that no harm is made to your test database.

